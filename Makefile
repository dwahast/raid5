CC=gcc
CXX=g++

CFLAGS = -g -O1 -std=c99 -lm -Wextra -Werror #-Wall


all: run_aluno grade

raidfs.o: raidfs.c

test.o: test.c

aluno.o: aluno.c

run_aluno: raidfs.o aluno.o
	$(CC) $(CFLAGS) raidfs.o aluno.o -o aluno -lm

test: raidfs.o test.o
	$(CC) $(CFLAGS) raidfs.o test.o -o test -lm

aluno: run_aluno
	./aluno

grade: test
	./test

clean:
	rm -rf *.o test aluno *.bin1 *.bin2 *.bin3 testes/*.bin1 testes/*.bin2 testes/*.bin3


