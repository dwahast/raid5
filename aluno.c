#include "raidfs.h"
#include "simplegrade.h"
#include <stdio.h>

inline void CLEANUP(const char * arquivo){
	int i;
	for(i=1;i<=3;i++){
		char nome[1024];
		sprintf(nome,"%s.bin%d",arquivo,i);
		remove((const char *)nome);		
	}
	
}

void test_arquivos_maiores(){
	int i;

	DESCRIBE("Testes de operação com arquivos maiores");

	IF("Crio um arquivo com 4 blocos e leio");
	THEN("Deve ler valor original");
	CLEANUP("testes/2048B");
	if (initfs("testes/2048B")==SUCESSO){
		indice_arq_t f = f_open("testes/2048B",LEITURA);
		char buffer[2048];
		if (f != FALHA){
			if (f_read(f, 2048, buffer)==2048){
				for(i=0; (i<2048) && (buffer[i]=='B'); i++);
				isEqual(i,2048,1); // para antes se não for B e falha
			} else isEqual(0,1,1);
			f_delete(f);
		} else isEqual(0,1,1);
	} else isEqual(0,1,1);

	IF("Crio um arquivo com 8 blocos e leio");
	THEN("Deve ler valor original");
	CLEANUP("testes/4096B");
	if (initfs("testes/4096B")==SUCESSO){
		indice_arq_t f = f_open("testes/4096B",LEITURA);
		char buffer[4096];
		if (f != FALHA){
			if (f_read(f, 4096, buffer)==4096){
				for(i=0; (i<4096) && (buffer[i]=='B'); i++);
				isEqual(i,4096,1); // para antes se não for B e falha
			} else isEqual(0,1,1);
			f_delete(f);
		} else isEqual(0,1,1);
	} else isEqual(0,1,1);

	IF("Crio um arquivo com 8 blocos e leio");
	THEN("Deve ler valor original");
	CLEANUP("testes/8192B");
	if (initfs("testes/8192B")==SUCESSO){
		indice_arq_t f = f_open("testes/8192B",LEITURA);
		char buffer[8192];
		if (f != FALHA){
			if (f_read(f, 8192, buffer)==8192){
				for(i=0; (i<8192) && (buffer[i]=='B'); i++);
				isEqual(i,8192,1); // para antes se não for B e falha
			} else isEqual(0,1,1);
			f_delete(f);
		} else isEqual(0,1,1);
	} else isEqual(0,1,1);
}

void test_falhas(){
	
	int i;

	DESCRIBE("Testes de operação que geram falhas");

	IF("Crio um arquivo para leitura e tento escrever");
	THEN("Deve retornar falha");
	CLEANUP("testes/2048B");
	if (initfs("testes/2048B")==SUCESSO){
		indice_arq_t f = f_open("testes/2048B",LEITURA);
		char buffer[1];
		if (f != FALHA){
			if (!f_write(f, 1, buffer)){
				isEqual(1,1,1);
			} else isEqual(0,1,1);
			f_close(f);
		} else isEqual(0,1,1);
		
		IF("Crio um arquivo para escrita e tento ler");
		THEN("Deve retornar falha");
		f = f_open("testes/2048B",ESCRITA);
		if (f != FALHA){
			if(!f_read(f, 1, buffer)){
				isEqual(1,1,1);
			} else isEqual(0,1,1);	
			f_close(f);
		} else isEqual(0,1,1);

		WHEN("Apago dois arquivos (.bin1 e .bin2)");
		IF("Tento recuperar");
		THEN("Deve retornar falha");
		f = f_open("testes/2048B",ESCRITA);
		if (f != FALHA){
			remove((const char *)"testes/2048B.bin1");	
			remove((const char *)"testes/2048B.bin2");	
			if(!recover("testes/2048B.bin")){
				isEqual(1,1,1);
			} else isEqual(0,1,1);
			f_delete(f);
		} else isEqual(0,1,1);
	} else isEqual(0,1,1);

	if (initfs("testes/2048B")==SUCESSO){
		WHEN("Apago dois arquivos (.bin2 e .bin3)");
		IF("Tento recuperar");
		THEN("Deve retornar falha");
		indice_arq_t f = f_open("testes/2048B",ESCRITA);
		if (f != FALHA){
			remove((const char *)"testes/2048B.bin2");	
			remove((const char *)"testes/2048B.bin3");	
			if(!recover("testes/2048B.bin")){
				isEqual(1,1,1);
			} else isEqual(0,1,1);
			f_delete(f);
		} else isEqual(0,1,1);
	}

	if (initfs("testes/2048B")==SUCESSO){
		WHEN("Apago dois arquivos (.bin1 e .bin3)");
		IF("Tento recuperar");
		THEN("Deve retornar falha");
		indice_arq_t f = f_open("testes/2048B",ESCRITA);
		if (f != FALHA){
			remove((const char *)"testes/2048B.bin1");	
			remove((const char *)"testes/2048B.bin3");	
			if(!recover("testes/2048B")){
				isEqual(1,1,1);
			} else isEqual(0,1,1);
			f_delete(f);
		} else isEqual(0,1,1);
	}

}

void test_blocos_quebrados(){
	int i;
	
	IF("Crio um arquivo com 6966 bytes e leio");
	THEN("Deve ler valor original");
	CLEANUP("testes/6966X");
	if (initfs("testes/6966X")==SUCESSO){
		indice_arq_t f = f_open("testes/6966X",LEITURA);
		char buffer[6966];
		if (f != FALHA){
			if (f_read(f, 6966, buffer)==6966){
				for(i=0; (i<6966) && (buffer[i]=='X'); i++);
				isEqual(i,6966,1); // para antes se não for B e falha
			} else isEqual(0,1,1);
			f_delete(f);
		} else isEqual(0,1,1);
	} else isEqual(0,1,1);

	IF("Crio um arquivo com 6966 bytes e escrevo");
	THEN("Deve ler valor modificado");
	CLEANUP("testes/6966X");
	if (initfs("testes/6966X")==SUCESSO){
		indice_arq_t f = f_open("testes/6966X",ESCRITA);
		char buffer[7010];
		char escrita[44];
		for(int l = 0; l < 44; l++){
			escrita[l] = 'B';
		}
		if (f != FALHA){
			f_seek(f,6966);
			f_write(f,44,escrita);
			f_close(f);
			f = f_open("testes/6966X",LEITURA);
			if (f_read(f, 7010, buffer)==7010){
				for(i=0; (i<6966) && (buffer[i]=='X'); i++);
				isEqual(i,6966,1); // para antes se não for B e falha
				for(i=6966; (i<7010) && (buffer[i]=='B'); i++);
				isEqual(i,7010,1); // para antes se não for B e falha		
				f_close(f);
			} else isEqual(0,1,1);
			IF("REMOVO ARQUIVO .bin1");
			THEN("DEVE RECUPERAR");
			remove((const char *)"testes/6966X.bin1");
			
			if(recover("testes/6966X")){
				isEqual(1,1,1);	
			}else isEqual(0,1,1);

		} else isEqual(0,1,1);
	} else isEqual(0,1,1);
}


int main()
{
	test_arquivos_maiores();
	test_falhas();
	test_blocos_quebrados();
	GRADEME();
	if(grade==maxgrade)
		return 0;
	else return grade;
}