#include "raidfs.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//DOUGLAS WAHAST DA COSTA
//UFPEL
//15201350

typedef struct fs{
	char *nome;
	char *modo;
	int pos;
}fs_t;

static fs_t *fileSystem;
char *bufferMaster;
int numArchs = 0;
int count = 0;
int seekVal = 0;
char nullTag = -1;

void printStruct(){
	for(int i = 0; i < numArchs; i++){
		printf("fileSystem[%d]:\n\tNome:%s\n\tmodo:%s\n",i,fileSystem[i].nome,fileSystem[i].modo);
	}
}

int findFileHandle(char *nome){
	for(int i = 1; i <= numArchs; i++){
		if(fileSystem[i-1].nome == nome){
			return i;
		}
	}
	return 0;
}

int initfs(char * arquivo){
	
	seekVal = 0;
	char bin1[strlen(arquivo)+5];
	char bin2[strlen(arquivo)+5];
	char bin3[strlen(arquivo)+5];
	strcpy(bin1,arquivo);
	strcat(bin1,".bin");
	FILE *fp = fopen(bin1,"r");
	if(fp){
	//.bin1 .bin2 .bin3 create
		strcpy(bin1,arquivo);
		strcpy(bin2,arquivo);
		strcpy(bin3,arquivo);
		strcat(bin1,".bin1");
		strcat(bin2,".bin2");
		strcat(bin3,".bin3");
		if(!fopen(bin1,"w"))
			return FALHA;
		if(!fopen(bin2,"w"))
			return FALHA;
		if(!fopen(bin3,"w"))
			return FALHA;
	//preenchimento
		FILE *fp1 = fopen(bin1,"w");
		FILE *fp2 = fopen(bin2,"w");
		FILE *fp3 = fopen(bin3,"w");
		char *buffer1 = calloc(512,sizeof(char));
		char *buffer2 = calloc(512,sizeof(char));
		char *buffer3 = calloc(512,sizeof(char));
		count = 0;
		while(!feof(fp)){
			//bloco 1
			for(int i = 0; i < 512;i++){
				buffer1[i] = fgetc(fp);	
				if(feof(fp))
					buffer1[i] = 0;
			}			
			//bloco 2				
			for(int i = 0; i < 512;i++){
				buffer2[i] = fgetc(fp);
				if(feof(fp))
					buffer2[i] = 0;					
			}
			//calculo paridade. Se null(0) preenche com -1, para ocupar os espaços
			for(int i = 0; i < 512;i++){				

				buffer3[i] = buffer1[i]^buffer2[i];				
				
				if(buffer1[i] == 0)
					buffer1[i] = nullTag;
				if(buffer2[i] == 0)
					buffer2[i] = nullTag;
				
				if((int)buffer3[i] == 0){
					buffer3[i] = nullTag;
				}
			}
			//switch para posicao da paridade na insercao no arquivo
			switch(count){
				//pos parity
				case 0:
					fputs(buffer1,fp1);
					fputs(buffer2,fp2);
					fputs(buffer3,fp3);
					break;
				case 1:
					fputs(buffer1,fp1);
					fputs(buffer2,fp3);
					fputs(buffer3,fp2);
					break;
				case 2:
					fputs(buffer1,fp2);
					fputs(buffer2,fp3);
					fputs(buffer3,fp1);
					break;
			}
			count++;
			if(count == 3)
				count = 0;

			if(feof(fp)){
				break;
			}
		}
		fclose(fp);
		fclose(fp1);
		fclose(fp2);
		fclose(fp3);
		return SUCESSO;	
	}else{
		return FALHA;
	}
}

indice_arq_t f_open(char * nome,  int acesso){
	char *modo;
	int i = 0;
	count = 0;
	seekVal = 0;
	char bin[strlen(nome)+5];
	if(acesso == LEITURA){
		modo = "rb";
	}else{
		if(acesso == ESCRITA){
			modo = "wb";
		}else{
			//ESCRITA E LEITURA
			modo = "w+b";
		}
	}
	int fh = findFileHandle(nome);
	if(fh != 0){
		fileSystem[fh-1].modo = modo;
		if(acesso!=LEITURAESCRITA)
			fileSystem[numArchs].pos = 0;
		return fh;
	}else{
		fileSystem = realloc(fileSystem ,(numArchs+1) * sizeof(fs_t));
		fileSystem[numArchs].nome = nome;
		fileSystem[numArchs].modo = modo;
		if(acesso!=LEITURAESCRITA)
			fileSystem[numArchs].pos = 0;
		
		numArchs++;	
		fh = findFileHandle(nome);
		//arquivo bin
		strcpy(bin,fileSystem[fh-1].nome);
		strcat(bin,".bin");
		FILE *p = fopen(bin,"rb");
		fseek(p, 0L, SEEK_END);
		int sz = ftell(p);
		if(sz == -1L)
			return FALHA;
		
		if(bufferMaster){
			free(bufferMaster);
		}
		bufferMaster = calloc(sz,sizeof(char));
		rewind(p);
		for(i = 0; i < sz; i++)	
			bufferMaster[i] = fgetc(p);

		fclose(p);	
		if(fh != 0){
			return fh;
		}else{
			return FALHA;	
		}
	}
}
int putIntofile(indice_arq_t arquivo){
	int i = 0;
	count = 0;
	char bin1[strlen(fileSystem[arquivo-1].nome)+5];
	char bin2[strlen(fileSystem[arquivo-1].nome)+5];
	char bin3[strlen(fileSystem[arquivo-1].nome)+5];
	strcpy(bin1,fileSystem[arquivo-1].nome);
	//.bin1 .bin2 .bin3 create
		strcpy(bin2,bin1);
		strcpy(bin3,bin1);
		strcat(bin1,".bin1");
		strcat(bin2,".bin2");
		strcat(bin3,".bin3");		

		FILE *fp1 = fopen(bin1,"wb");
		FILE *fp2 = fopen(bin2,"wb");
		FILE *fp3 = fopen(bin3,"wb");
	//preenchimento
		char *buffer1 = calloc(512,sizeof(char));
		char *buffer2 = calloc(512,sizeof(char));
		char *buffer3 = calloc(512,sizeof(char));

	while(i < (int)strlen(bufferMaster)){
		//bloco1
		for(int l = 0; l < 512; l++){
			buffer1[l] = bufferMaster[i];
			i++;
		}
		//bloco2
		for(int l = 0; l < 512; l++){
			buffer2[l] = bufferMaster[i];
			i++;
		}
		//paridade calc
		for(int l = 0; l < 512; l++){
			buffer3[l] = buffer1[i]^buffer2[i];
			if(buffer3[l]==0){
				buffer3[l] = nullTag;
			}
		}
		switch(count){
			case 0:
					fputs(buffer1,fp1);
					fputs(buffer2,fp2);
					fputs(buffer3,fp3);
					break;
				case 1:
					fputs(buffer1,fp1);
					fputs(buffer2,fp3);
					fputs(buffer3,fp2);					
					break;
				case 2:
					fputs(buffer1,fp2);
					fputs(buffer2,fp3);
					fputs(buffer3,fp1);
					break;
		}
		count++;
		if(count == 3)
			count = 0;
	}
	fclose(fp1);
	fclose(fp2);
	fclose(fp3);

	return SUCESSO;
}


int f_close(indice_arq_t arquivo){
	int k = 0;
	
	if(fileSystem[arquivo-1].modo == "wb" || fileSystem[arquivo-1].modo == "w+b"){//so se foi arquivo de escrita
		if(!putIntofile(arquivo))
			return FALHA;
	}
	fs_t *aux = calloc(numArchs-1,sizeof(fs_t));
	if(!aux)
		return FALHA;

	for(int i = 0, k = 0; i < numArchs; i++){
		if(i == arquivo-1){
			i++;
		}

		aux[k].nome = fileSystem[i].nome;
		aux[k].modo = fileSystem[i].modo;
		aux[k].pos = fileSystem[i].pos;
		k++;
	}
	seekVal = 0;
	count = 0;
	numArchs--;
	free(fileSystem);
	fileSystem = aux;
	return SUCESSO;
}

uint32_t f_read(indice_arq_t arquivo, uint32_t tamanho, char *buffer){
	int i = 0;
	int k = 0;
	int read = 0;
	int until = (int)tamanho + fileSystem[arquivo-1].pos;

	if(fileSystem[arquivo-1].modo == "wb")
		return FALHA;

	char aux[strlen(fileSystem[arquivo-1].nome)+4];
	char bin1[strlen(fileSystem[arquivo-1].nome)+5];
	char bin2[strlen(fileSystem[arquivo-1].nome)+5];
	char bin3[strlen(fileSystem[arquivo-1].nome)+5];
	strcpy(aux,fileSystem[arquivo-1].nome);
	strcpy(bin1,aux);
	strcpy(bin2,aux);
	strcpy(bin3,aux);
	strcat(bin1,".bin1");
	strcat(bin2,".bin2");
	strcat(bin3,".bin3");
	FILE *fp1 = fopen(bin1,"rb");
	FILE *fp2 = fopen(bin2,"rb");
	FILE *fp3 = fopen(bin3,"rb");

	if(seekVal>0){//se arquivo ja foi lido antes
		fseek(fp1,(long int)seekVal,SEEK_SET);
		fseek(fp2,(long int)seekVal,SEEK_SET);
		fseek(fp3,(long int)seekVal,SEEK_SET);
		seekVal += 512;
	}		

	while(fileSystem[arquivo-1].pos < until){

		switch(count){
			case 0:	
				fseek(fp3,seekVal+512,SEEK_SET);
				for(int l = 0; l < 512;l++){
					buffer[i] = fgetc(fp1);
					if(buffer[i]!=nullTag){
						read++;	
					}
					i++;
					k++;
					fileSystem[arquivo-1].pos++;
					if(i == (int)tamanho){
						break;
					}
				}
				for(int l = 0; l < 512;l++){
					buffer[i] = fgetc(fp2);
					if(buffer[i]!=nullTag){
						read++;
					}
					i++;
					k++;
					fileSystem[arquivo-1].pos++;
					if(i == (int)tamanho){
						break;
					}
				}
				break;	

			case 1:
	
				fseek(fp2,seekVal+512,SEEK_SET);
				for(int l = 0; l < 512;l++){
					buffer[i] = fgetc(fp1);
					if(buffer[i]!=nullTag){
						read++;
					}
					i++;
					k++;
					fileSystem[arquivo-1].pos++;
					if(i == (int)tamanho){
						break;
					}
				}
				for(int l = 0; l < 512;l++){
					buffer[i] = fgetc(fp3);
					if(buffer[i]!=nullTag){
						read++;
					}
					i++;
					k++;
					fileSystem[arquivo-1].pos++;
					if(i == (int)tamanho)
						break;
				}
				break;	

			case 2:
				fseek(fp1,seekVal+512,SEEK_SET);
				for(int l = 0; l < 512;l++){
					buffer[i] = fgetc(fp2);
					if(buffer[i]!=nullTag){
						read++;
					}
					i++;
					k++;
					fileSystem[arquivo-1].pos++;
					if(i == (int)tamanho)
						break;
					
				}
				for(int l = 0; l < 512;l++){
					buffer[i] = fgetc(fp3);
					if(buffer[i]!=nullTag){
						read++;
					}
					i++;
					k++;
					fileSystem[arquivo-1].pos++;
					if(i == (int)tamanho)
						break;
				}
				break;	
		}
		seekVal = seekVal + 512;
		if(k==1024){
			k = 0;
			count++;
			if(count == 3){
				count = 0;	
			}
		}		
	}
	return read;
}

int f_write(indice_arq_t arquivo, uint32_t tamanho, char *buffer){
	if(fileSystem[arquivo-1].modo == "rb")
		return FALHA;
		
	int from = fileSystem[arquivo-1].pos;
	int until = from + (int)tamanho;
	char *aux = calloc(strlen(bufferMaster)+tamanho,sizeof(char));
	for(int i = 0; i < from; i++){
		aux[i] = bufferMaster[i];
	}
	for(int i = from; i < until;i++){
		for(int k = 0;k < (int)tamanho;k++){
			aux[i] = buffer[k];
			i++;
		}	
	
	} 
	free(bufferMaster);
	bufferMaster = calloc(until,sizeof(char));
	bufferMaster = aux;	
	fileSystem[arquivo-1].pos+=tamanho;

	return SUCESSO;
}


int f_delete(indice_arq_t arquivo){
	
	char bins[(int)strlen(fileSystem[arquivo-1].nome) + 5];

	strcpy(bins,fileSystem[arquivo-1].nome);
	strcat(bins,".bin1");
	remove((const char *)bins);	
	strcpy(bins,fileSystem[arquivo-1].nome);
	strcat(bins,".bin2");
	remove((const char *)bins);	
	strcpy(bins,fileSystem[arquivo-1].nome);
	strcat(bins,".bin3");
	remove((const char *)bins);	
	
	if(!f_close(arquivo))
		return FALHA;

	return SUCESSO;
}


int f_seek(indice_arq_t arquivo, uint32_t seek){
	if((int)seek >= 0){
		fileSystem[arquivo-1].pos = seek;
		return SUCESSO;	
	}else{
		return FALHA;
	}

}

int recover(char * nome){
	
	FILE *f1;
	FILE *f2;
	FILE *f3;
	int lost = 0;
	int sz = 0;
	char bin1[strlen(nome)+5];
	char bin2[strlen(nome)+5];
	char bin3[strlen(nome)+5];
	strcpy(bin1,nome);
	strcat(bin1,".bin1");
	strcpy(bin2,nome);
	strcat(bin2,".bin2");
	strcpy(bin3,nome);
	strcat(bin3,".bin3");
	if(!(f1 = fopen(bin1,"rb"))){
		lost = 1;
		if(!(f2 = fopen(bin2,"rb"))){
			return FALHA;
		}else{
			if(!(f3 = fopen(bin3,"rb"))){
				return FALHA;
			}
		}
	}else{		
		if(!(f2 = fopen(bin2,"rb"))){
			lost = 2;
			if(!(f1 = fopen(bin1,"rb"))){
				return FALHA;
			}else{
				if(!(f3 = fopen(bin3,"rb"))){
					return FALHA;
				}
			}
		}else{
			if(!(f3 = fopen(bin3,"rb"))){
				lost = 3;
			}else{
				if(!(f1 = fopen(bin1,"rb"))){
					return FALHA;
				}else{
					if(!(f2 = fopen(bin2,"rb"))){
						return FALHA;
					}
				}			
			}		
		}
	}	
	char *buffer1;
	char *buffer2;
	char *buffer3;
	//recuperacao
	switch(lost){
		
		case 1:
	
			f1 = fopen(bin1,"wb");
			f2 = fopen(bin2,"rb");
			f3 = fopen(bin3,"rb");
		
			fseek(f2,0,SEEK_END);
			sz = ftell(f2);
			rewind(f2);
			
			buffer1 = calloc(sz,sizeof(char));
			buffer2 = calloc(sz,sizeof(char));
			buffer3 = calloc(sz,sizeof(char));
			for(int i = 0; i < sz;i++){
				buffer1[i] = fgetc(f2);
			}
			for(int i = 0; i < sz;i++){
				buffer2[i] = fgetc(f3);
			}
			for(int i = 0; i < sz;i++){
				
				if(buffer1[i] == nullTag)					
					buffer1[i] = 0;
				if(buffer2[i] == nullTag)					
					buffer2[i] = 0;
						
				buffer3[i] = buffer1[i]^buffer2[i];

				if(buffer3[i] == 0)
					buffer3[i] = nullTag;
			}
			fputs(buffer3,f1);
			free(buffer1);
			free(buffer2);
			free(buffer3);
			break;
		
		case 2:
			
			f1 = fopen(bin1,"rb");
			f2 = fopen(bin2,"wb");
			f3 = fopen(bin3,"rb");
	
			fseek(f1,0,SEEK_END);
			sz = ftell(f1);
			rewind(f1);

			buffer1 = calloc(sz,sizeof(char));
			buffer2 = calloc(sz,sizeof(char));
			buffer3 = calloc(sz,sizeof(char));

			for(int i = 0; i < sz;i++){
				buffer1[i] = fgetc(f1);
			}
			for(int i = 0; i < sz;i++){
				buffer2[i] = fgetc(f3);
			}
			for(int i = 0; i < sz;i++){
				
				if(buffer1[i] == nullTag)					
					buffer1[i] = 0;
				if(buffer2[i] == nullTag)					
					buffer2[i] = 0;
						
				buffer3[i] = buffer1[i]^buffer2[i];

				if(buffer3[i] == 0)
					buffer3[i] = nullTag;
			}
			fputs(buffer3,f2);
			free(buffer1);
			free(buffer2);
			free(buffer3);

			break;
		
		case 3:

			f1 = fopen(bin1,"rb");
			f2 = fopen(bin2,"rb");
			f3 = fopen(bin3,"wb");
	
			fseek(f1,0,SEEK_END);
			sz = ftell(f1);
			rewind(f1);
			
			buffer1 = calloc(sz,sizeof(char));
			buffer2 = calloc(sz,sizeof(char));
			buffer3 = calloc(sz,sizeof(char));

			for(int i = 0; i < sz;i++){
				buffer1[i] = fgetc(f1);
			}
			for(int i = 0; i < sz;i++){
				buffer2[i] = fgetc(f2);
			}
			for(int i = 0; i < sz;i++){
				
				if(buffer1[i] == nullTag)					
					buffer1[i] = 0;
				if(buffer2[i] == nullTag)					
					buffer2[i] = 0;
						
				buffer3[i] = buffer1[i]^buffer2[i];

				if(buffer3[i] == 0)
					buffer3[i] = nullTag;
			}

			fputs(buffer3,f3);
			free(buffer1);
			free(buffer2);
			free(buffer3);

			break;	

	}
	fclose(f1);
	fclose(f2);
	fclose(f3);
	return SUCESSO;
}